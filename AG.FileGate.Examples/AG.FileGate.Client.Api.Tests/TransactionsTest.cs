﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AG.FileGate.Interaction.Model;
using AG.FileGate.Interaction.Results;
using AG.FileGate.TestUtilities;
using AG.FileGate.Utilities;
using AG.FileGate.Utilities.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AG.FileGate.Client.Api.Tests
{
    [TestClass]
    public class TransactionsTest
    {
        private const string smallFileName = "small.txt";
        private Transactions _transactions;
        [TestInitialize]
        public void Init()
        {
            Tcp.Configuration clientConfig = new Tcp.Configuration(ConfigurationUtils.GetXmlConfigurationFromBaseDirectory("client.xml"));
            _transactions = new Transactions(clientConfig);
        }

        [TestMethod]
        public void TestServerInfoTransaction()
        {
            string result = _transactions.GetServerInfo();
            Assert.IsFalse(String.IsNullOrEmpty(result));
        }

        [TestMethod]
        public void TestGetItems()
        {
            string dirName = Guid.NewGuid().ToString();
            string serverRelativeUrl = $"/{dirName}";

            _transactions.AddDirectory(serverRelativeUrl);

            FileStorageItemCollection items = _transactions.GetItems(new Query
            {
                Where = "ServerRelativeUrl=/&SearchPattern=*"
            });
            Assert.IsNotNull(items);
            Assert.IsTrue(items.PagingInfo.CurrentRowCount > 0);
        }

        [TestMethod]
        public void TestGetItemByKey()
        {
            string dirName = Guid.NewGuid().ToString();
            string serverRelativeUrl = $"/{dirName}";
            string fileName = Path.GetRandomFileName();
            string serverRelativeUrl2 = $"/{dirName}/{fileName}";
            
            _transactions.AddDirectory(serverRelativeUrl);

            FileStorageItem item = _transactions.GetItemByKey(serverRelativeUrl);
            Assert.IsNotNull(item);
            Assert.IsTrue(item.FileStorageItemType == FileStorageItemTypes.Dir);

            AddSmallFile(serverRelativeUrl2);

            FileStorageItem file = _transactions.GetItemByKey(serverRelativeUrl2);
            Assert.IsNotNull(file);
            Assert.IsTrue(file.FileStorageItemType == FileStorageItemTypes.File);
        }

        [TestMethod]
        public void TestAddDirectory()
        {
            string dirName = Guid.NewGuid().ToString();
            string serverRelativeUrl = $"/{dirName}";

            BooleanTransactionResult result = _transactions.AddDirectory(serverRelativeUrl);
            Assert.IsTrue(result.Result);
            Assert.IsTrue(String.IsNullOrEmpty(result.Error));

            FileStorageItemCollection items = _transactions.GetItems(new Query
            {
                Where = "ServerRelativeUrl=/&SearchPattern=" + dirName
            });
            Assert.IsNotNull(items);
            Assert.IsTrue(items.Count() > 0);
        }

        [TestMethod]
        public void TestAddFile()
        {
            string dirName = Guid.NewGuid().ToString();
            string fileName = Path.GetRandomFileName();
            string serverRelativeUrl = $"/{dirName}/{fileName}";

            string testedFile = DataUtils.GetFileNameFromBaseDirectory(smallFileName);
            FileInfo f = new FileInfo(testedFile);

            BooleanTransactionResult result = AddSmallFile(serverRelativeUrl);
            Assert.IsTrue(result.Result);
            Assert.IsTrue(String.IsNullOrEmpty(result.Error));

            FileStorageItemCollection items = _transactions.GetItems(new Query
            {
                Where = "ServerRelativeUrl=" + dirName + "&SearchPattern=" + fileName
            });
            Assert.IsNotNull(items);
            Assert.IsTrue(items.Count() > 0);
            Assert.IsTrue(items.First().Length > 0);
            decimal i = items.First().Length * 1024 * 1024;
            Assert.IsTrue(i == f.Length);
        }

        [TestMethod]
        public void TestDeleteDirectory()
        {
            string dirName = Guid.NewGuid().ToString();
            string serverRelativeUrl = $"/{dirName}";

            _transactions.AddDirectory(serverRelativeUrl);

            BooleanTransactionResult result = _transactions.DeleteDirectory(serverRelativeUrl);
            Assert.IsTrue(result.Result);
            Assert.IsTrue(String.IsNullOrEmpty(result.Error));

            FileStorageItemCollection items = _transactions.GetItems(new Query
            {
                Where = "ServerRelativeUrl=/&SearchPattern=" + dirName
            });
            Assert.IsNotNull(items);
            Assert.IsTrue(items.Count() == 0);
        }

        [TestMethod]
        public void TestDeleteFile()
        {
            string dirName = Guid.NewGuid().ToString();
            string fileName = Path.GetRandomFileName();
            string serverRelativeUrl = $"/{dirName}/{fileName}";

            AddSmallFile(serverRelativeUrl);

            BooleanTransactionResult result = _transactions.DeleteFile(serverRelativeUrl);
            Assert.IsTrue(result.Result);
            Assert.IsTrue(String.IsNullOrEmpty(result.Error));

            FileStorageItemCollection items = _transactions.GetItems(new Query
            {
                Where = "ServerRelativeUrl=" + dirName + "&SearchPattern=" + fileName
            });
            Assert.IsNotNull(items);
            Assert.IsTrue(items.Count() == 0);
        }

        [TestMethod]
        public void TestUpdateDirectory()
        {
            string dirName = Guid.NewGuid().ToString();
            string serverRelativeUrl = $"/{dirName}";

            string newDirName = Guid.NewGuid().ToString();
            string newServerRelativeUrl = $"/{newDirName}";

            _transactions.AddDirectory(serverRelativeUrl);

            Dictionary<string, object> updatedProperties = new Dictionary<string, object>
            {
                {"ServerRelativeUrl", newServerRelativeUrl}
            };

            BooleanTransactionResult result = _transactions.UpdateDirectory(serverRelativeUrl, updatedProperties);
            Assert.IsTrue(result.Result);
            Assert.IsTrue(String.IsNullOrEmpty(result.Error));

            FileStorageItemCollection items1 = _transactions.GetItems(new Query
            {
                Where = "ServerRelativeUrl=/&SearchPattern=" + dirName
            });
            Assert.IsNotNull(items1);
            Assert.IsTrue(items1.Count() == 0);

            FileStorageItemCollection items2 = _transactions.GetItems(new Query
            {
                Where = "ServerRelativeUrl=/&SearchPattern=" + newDirName
            });
            Assert.IsNotNull(items2);
            Assert.IsTrue(items2.Count() > 0);
        }

        [TestMethod]
        public void TestUpdateFile()
        {
            string dirName = Guid.NewGuid().ToString();
            string fileName = Path.GetRandomFileName();
            string serverRelativeUrl = $"/{dirName}/{fileName}";

            string newDirName = Guid.NewGuid().ToString();
            string newFileName = Path.GetRandomFileName();
            string newServerRelativeUrl = $"/{newDirName}/{newFileName}";

            AddSmallFile(serverRelativeUrl);

            Dictionary<string, object> updatedProperties = new Dictionary<string, object>
            {
                {"ServerRelativeUrl", newServerRelativeUrl}
            };

            BooleanTransactionResult result = _transactions.UpdateFile(serverRelativeUrl, updatedProperties);
            Assert.IsTrue(result.Result);
            Assert.IsTrue(String.IsNullOrEmpty(result.Error));

            FileStorageItemCollection items1 = _transactions.GetItems(new Query
            {
                Where = "ServerRelativeUrl=" + dirName + "&SearchPattern=" + fileName
            });
            Assert.IsNotNull(items1);
            Assert.IsTrue(items1.Count() == 0);

            FileStorageItemCollection items2 = _transactions.GetItems(new Query
            {
                Where = "ServerRelativeUrl=" + newDirName + "&SearchPattern=" + newFileName
            });
            Assert.IsNotNull(items2);
            Assert.IsTrue(items2.Count() > 0);
        }

        [TestMethod]
        public void TestExistDirectory()
        {
            string dirName = Guid.NewGuid().ToString();
            string serverRelativeUrl = $"/{dirName}";

            string newDirName = Guid.NewGuid().ToString();
            string newServerRelativeUrl = $"/{newDirName}";

            _transactions.AddDirectory(serverRelativeUrl);

            FileStorageItemCollection items1 = _transactions.GetItems(new Query
            {
                Where = "ServerRelativeUrl=/&SearchPattern=" + dirName
            });
            Assert.IsNotNull(items1);
            Assert.IsTrue(items1.Count() > 0);

            FileStorageItemCollection items2 = _transactions.GetItems(new Query
            {
                Where = "ServerRelativeUrl=/&SearchPattern=" + newDirName
            });
            Assert.IsNotNull(items2);
            Assert.IsTrue(items2.Count() == 0);

            BooleanTransactionResult result1 = _transactions.ExistDirectory(serverRelativeUrl);
            Assert.IsTrue(result1.Result);

            BooleanTransactionResult result2 = _transactions.ExistDirectory(newServerRelativeUrl);
            Assert.IsFalse(result2.Result);
        }

        [TestMethod]
        public void TestExistFile()
        {
            string dirName = Guid.NewGuid().ToString();
            string fileName = Path.GetRandomFileName();
            string serverRelativeUrl = $"/{dirName}/{fileName}";

            string newDirName = Guid.NewGuid().ToString();
            string newFileName = Path.GetRandomFileName();
            string newServerRelativeUrl = $"/{newDirName}/{newFileName}";

            AddSmallFile(serverRelativeUrl);

            FileStorageItemCollection items1 = _transactions.GetItems(new Query
            {
                Where = "ServerRelativeUrl=" + dirName + "&SearchPattern=" + fileName
            });
            Assert.IsNotNull(items1);
            Assert.IsTrue(items1.Count() > 0);

            FileStorageItemCollection items2 = _transactions.GetItems(new Query
            {
                Where = "ServerRelativeUrl=" + newDirName + "&SearchPattern=" + newFileName
            });
            Assert.IsNotNull(items2);
            Assert.IsTrue(items2.Count() == 0);

            BooleanTransactionResult result1 = _transactions.ExistFile(serverRelativeUrl);
            Assert.IsTrue(result1.Result);

            BooleanTransactionResult result2 = _transactions.ExistFile(newServerRelativeUrl);
            Assert.IsFalse(result2.Result);
        }

        [TestMethod]
        public void TestGetHealth()
        {
            uint health = _transactions.GetHealth();
            Assert.IsTrue(health > 0);
        }

        private BooleanTransactionResult AddSmallFile(string serverRelativeUrl)
        {
            string testedFile = DataUtils.GetFileNameFromBaseDirectory(smallFileName);
            using (Stream fs = File.OpenRead(testedFile))
            {
                return _transactions.AddFile(serverRelativeUrl, fs);
            }
        }
    }
}
