﻿using System;
using System.Collections.Generic;
using System.IO;
using AG.FileGate.Client.Tcp;
using AG.FileGate.Client.Tcp.Transactions.FileStreamStorage;
using AG.FileGate.Interaction.Contracts;
using AG.FileGate.Interaction.Model;
using AG.FileGate.Interaction.Results;
using AG.FileGate.Interaction.Tcp.Transactions;
using AG.FileGate.Utilities;

namespace AG.FileGate.Client.Api
{
    public class Transactions
    {
        private readonly Tcp.Configuration _clientConfig;

        public Transactions(Tcp.Configuration clientConfig)
        {
            _clientConfig = clientConfig;
        }

        public BooleanTransactionResult AddDirectory(string serverRelativeUrl)
        {
            TransactionResult result;

            Contracts.Client externalClient = new NetClient(_clientConfig);
            externalClient.EstablishConnection();
            try
            {
                Tcp.Transactions.RootTransactionsFactory factory = new Tcp.Transactions.RootTransactionsFactory();
                ClientNetTransaction fileStreamAddDirectoryTransaction =
                    factory.MakeAddDirectoryTransaction(serverRelativeUrl);

                result = externalClient.Execute(fileStreamAddDirectoryTransaction);
            }
            finally
            {
                externalClient.CloseConnection();
            }

            return (BooleanTransactionResult)result;
        }

        public BooleanTransactionResult AddFile(string serverRelativeUrl, Stream file)
        {
            TransactionResult result;

            Contracts.Client externalClient = new NetClient(_clientConfig);
            externalClient.EstablishConnection();
            try
            {
                Tcp.Transactions.RootTransactionsFactory factory =
                    new Tcp.Transactions.RootTransactionsFactory();
                ClientNetTransaction fileStreamAddFileTransaction =
                    factory.MakeAddFileTransaction(serverRelativeUrl, file);

                result = externalClient.Execute(fileStreamAddFileTransaction);
            }
            finally
            {
                externalClient.CloseConnection();
            }

            return (BooleanTransactionResult)result;
        }

        public BooleanTransactionResult DeleteDirectory(string serverRelativeUrl)
        {
            TransactionResult result;

            Contracts.Client externalClient = new NetClient(_clientConfig);
            externalClient.EstablishConnection();

            try
            {
                Tcp.Transactions.RootTransactionsFactory factory =
                    new Tcp.Transactions.RootTransactionsFactory();
                ClientNetTransaction fileStreamDeleteDirectoryTransaction =
                    factory.MakeDeleteDirectoryTransaction(serverRelativeUrl);

                result = externalClient.Execute(fileStreamDeleteDirectoryTransaction);
            }
            finally
            {
                externalClient.CloseConnection();
            }

            return (BooleanTransactionResult)result;
        }

        public BooleanTransactionResult DeleteFile(string serverRelativeUrl)
        {
            TransactionResult result;

            Contracts.Client externalClient = new NetClient(_clientConfig);
            externalClient.EstablishConnection();

            try
            {
                Tcp.Transactions.RootTransactionsFactory factory =
                    new Tcp.Transactions.RootTransactionsFactory();
                ClientNetTransaction fileStreamDeleteFileTransaction =
                    factory.MakeDeleteFileTransaction(serverRelativeUrl);

                result = externalClient.Execute(fileStreamDeleteFileTransaction);
            }
            finally
            {
                externalClient.CloseConnection();
            }

            return (BooleanTransactionResult) result;
        }

        public BooleanTransactionResult UpdateDirectory(string serverRelativeUrl,
            Dictionary<string, object> updatedProperties)
        {
            TransactionResult result;

            Contracts.Client externalClient = new NetClient(_clientConfig);
            externalClient.EstablishConnection();

            try
            {
                Tcp.Transactions.RootTransactionsFactory factory =
                    new Tcp.Transactions.RootTransactionsFactory();
                ClientNetTransaction fileStreamUpdateDirectoryTransaction =
                    factory.MakeUpdateDirectoryTransaction(serverRelativeUrl, updatedProperties);

                result = externalClient.Execute(fileStreamUpdateDirectoryTransaction);
            }
            finally
            {
                externalClient.CloseConnection();
            }

            return (BooleanTransactionResult) result;
        }

        public BooleanTransactionResult UpdateFile(string serverRelativeUrl,
            Dictionary<string, object> updatedProperties)
        {
            TransactionResult result;

            Contracts.Client externalClient = new NetClient(_clientConfig);
            externalClient.EstablishConnection();

            try
            {
                Tcp.Transactions.RootTransactionsFactory factory =
                    new Tcp.Transactions.RootTransactionsFactory();
                ClientNetTransaction fileStreamUpdateFileTransaction =
                    factory.MakeUpdateFileTransaction(serverRelativeUrl, updatedProperties);

                result =
                    externalClient.Execute(fileStreamUpdateFileTransaction);
            }
            finally
            {
                externalClient.CloseConnection();
            }

            return (BooleanTransactionResult) result;
        }

        public FileStorageItemCollection GetItems(Query query)
        {
            FileStorageItemCollection items;

            Contracts.Client externalClient = new NetClient(_clientConfig);
            externalClient.EstablishConnection();
            try
            {
                Tcp.Transactions.RootTransactionsFactory factory =
                    new Tcp.Transactions.RootTransactionsFactory();
                ClientNetTransaction fileStreamGetItemsTransaction =
                    factory.MakeGetItemsTransaction(query);

                TransactionResult result = externalClient.Execute(fileStreamGetItemsTransaction);
                
                items = (FileStorageItemCollection) ((ListTransactionResult<FileStorageItem>)result).Result;
            }
            finally
            {
                externalClient.CloseConnection();
            }

            return items;
        }
        
        public FileStorageItem GetItemByKey(string serverRelativeUrl)
        {
            FileStorageItem item;

            Contracts.Client externalClient = new NetClient(_clientConfig);
            externalClient.EstablishConnection();
            try
            {
                Tcp.Transactions.RootTransactionsFactory factory =
                    new Tcp.Transactions.RootTransactionsFactory();
                ClientNetTransaction fileStreamGetItemByKeyTransaction =
                    factory.MakeGetItemByKey(serverRelativeUrl);

                TransactionResult result = externalClient.Execute(fileStreamGetItemByKeyTransaction);
                item = ((ItemTransactionResult<FileStorageItem>)result).Result;
            }
            finally
            {
                externalClient.CloseConnection();
            }

            return item;
        }

        public BooleanTransactionResult ExistDirectory(string serverRelativeUrl)
        {
            TransactionResult result;

            Contracts.Client externalClient = new NetClient(_clientConfig);
            externalClient.EstablishConnection();
            try
            {
                Tcp.Transactions.RootTransactionsFactory factory =
                    new Tcp.Transactions.RootTransactionsFactory();
                ClientNetTransaction fileStreamExistDirectoryTransaction =
                    factory.MakeExistDirectoryTransaction(serverRelativeUrl);

                result = externalClient.Execute(fileStreamExistDirectoryTransaction);
            }
            finally
            {
                externalClient.CloseConnection();
            }

            return (BooleanTransactionResult) result;
        }

        public BooleanTransactionResult ExistFile(string serverRelativeUrl)
        {
            TransactionResult result;

            Contracts.Client externalClient = new NetClient(_clientConfig);
            externalClient.EstablishConnection();
            try
            {
                Tcp.Transactions.RootTransactionsFactory factory =
                    new Tcp.Transactions.RootTransactionsFactory();
                ClientNetTransaction fileStreamExistFileTransaction =
                    factory.MakeExistFileTransaction(serverRelativeUrl);

                result = externalClient.Execute(fileStreamExistFileTransaction);
            }
            finally
            {
                externalClient.CloseConnection();
            }

            return (BooleanTransactionResult) result;
        }

        public uint GetHealth()
        {
            Contracts.Client externalClient = new NetClient(_clientConfig);
            externalClient.EstablishConnection();
            try
            {
                Tcp.Transactions.RootTransactionsFactory factory =
                    new Tcp.Transactions.RootTransactionsFactory();
                ClientNetTransaction fileStreamAddDirectoryTransaction =
                    factory.MakeGetHealthTransaction();

                TransactionResult result = externalClient.Execute(fileStreamAddDirectoryTransaction);
                uint health = uint.Parse(((TextTransactionResult)result).Text);

                return health;
            }
            finally
            {
                externalClient.CloseConnection();
            }
        }

        public string GetServerInfo()
        {
            Contracts.Client externalClient = new NetClient(_clientConfig);
            externalClient.EstablishConnection();
            try
            {
                Tcp.Transactions.RootTransactionsFactory factory = new Tcp.Transactions.RootTransactionsFactory();
                TransactionResult result = externalClient.Execute(factory.MakeServerInfoTransaction());
                return ((TextTransactionResult)result).Text;
            }
            finally
            {
                externalClient.CloseConnection();
            }
        }

        public BooleanTransactionResult GetFileStream(string serverRelativeUrl, Action<byte[]> readPackage)
        {
            TransactionResult result;
            if (null == readPackage)
            {
                throw new NullReferenceException("Action is null");
            }

            Contracts.Client externalClient = new NetClient(_clientConfig);
            externalClient.EstablishConnection();
            try
            {
                Tcp.Transactions.RootTransactionsFactory factory =
                    new Tcp.Transactions.RootTransactionsFactory();
                ClientNetTransaction fileStreamAddDirectoryTransaction =
                    factory.MakeGetFileStream(serverRelativeUrl);

                ((GetFileStream) fileStreamAddDirectoryTransaction).OnReadPackage += (sender, bytes) =>
                {
                    readPackage(bytes);
                };

                result = externalClient.Execute(fileStreamAddDirectoryTransaction);
            }
            finally
            {
                externalClient.CloseConnection();
            }
            return (BooleanTransactionResult) result;
        }
    }
}
