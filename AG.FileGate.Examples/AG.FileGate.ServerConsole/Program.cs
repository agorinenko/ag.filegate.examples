﻿using System;
using System.Runtime.InteropServices;
using AG.FileGate.Configuration;
using AG.FileGate.TestUtilities;

namespace AG.FileGate.ServerConsole
{
    class Program
    {
        static Server.Contracts.Server _server;
        static void Main(string[] args)
        {
            try
            {
                handler = ConsoleEventCallback;
                SetConsoleCtrlHandler(handler, true);

                Console.WriteLine("Server is starting...");
                XmlConfiguration xmlConfiguration = ConfigurationUtils.GetXmlConfigurationFromBaseDirectory("server.xml");
                Configuration.Tcp.Configuration serverConfig = new Configuration.Tcp.Configuration(xmlConfiguration);
                _server = new Server.Tcp.NetServer(serverConfig);
                _server.OnConnectionOpen += (sender, eventArgs) =>
                {
                    Console.WriteLine("Open connection #" + eventArgs.ToString());
                };
                _server.OnConnectionClose += (sender, eventArgs) =>
                {
                    Console.WriteLine("Close connection #" + eventArgs.ToString());
                };
                _server.OnConnectionException += (sender, exception) =>
                {
                    Console.WriteLine(exception);
                };
                _server.Start();
                Console.WriteLine("Server started.");
                while (true)
                {
                    _server.AcceptClient();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                StopServer();
            }
        }

        private static void StopServer()
        {
            if (null != _server)
            {
                _server.Stop();
            }
        }

        static bool ConsoleEventCallback(int eventType)
        {
            if (eventType == 2)
            {
                StopServer();
            }
            return false;
        }

        static ConsoleEventDelegate handler; 
        private delegate bool ConsoleEventDelegate(int eventType);
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool SetConsoleCtrlHandler(ConsoleEventDelegate callback, bool add);
    }
}
