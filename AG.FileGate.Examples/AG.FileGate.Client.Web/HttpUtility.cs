﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Web;
using AG.FileGate.Client.Web.Models;

namespace AG.FileGate.Client.Web
{
    internal static class HttpUtility
    {
        public static ExceptionDataContract CreateExceptionDataContract(ErrorInfo ex)
        {
            Guid errorId = Guid.NewGuid();
            return new ExceptionDataContract
            {
                Message = ex.Message,
                Id = errorId
            };
        }

        public static void WriteResponse(HttpContext context, string text, int statusCode, string contentType)
        {
            context.Response.Clear();
            context.Response.ClearHeaders();
            context.Response.ClearContent();

            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            context.Response.BufferOutput = true;

            context.Response.ContentType = contentType;

            context.Response.StatusCode = statusCode;

            context.Response.Write(text);
            context.Response.Flush();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        public static string GetParam(this HttpContext context, string name)
        {
            string val = context.Request.Form[name];
            if (string.IsNullOrEmpty(val))
            {
                val = context.Request.QueryString[name];
            }

            return System.Web.HttpUtility.UrlDecode(val);
        }
        public static string GetRequiredParam(this HttpContext context, string name)
        {
            string val = context.GetParam(name);
            if (string.IsNullOrEmpty(val))
            {
                throw new NullReferenceException("Parameter \"" + name + "\" is null or empty");
            }

            return val;
        }

        public static string GetJson(Object o)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(o.GetType());

                ser.WriteObject(ms, o);
                ms.Position = 0;
                StreamReader sr = new StreamReader(ms);
                string json = sr.ReadToEnd();

                return json;
            }
        }
    }
}