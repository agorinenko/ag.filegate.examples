﻿using System;
using AG.FileGate.Client.Web.Models;

namespace AG.FileGate.Client.Web.Contracts
{
    public abstract class XmlHttpHandler : RestHttpHandler
    {
        protected override string ContentType => "application/xml";

        protected override string FormatException(ErrorInfo ex)
        {
            throw new NotImplementedException();
        }
    }
}