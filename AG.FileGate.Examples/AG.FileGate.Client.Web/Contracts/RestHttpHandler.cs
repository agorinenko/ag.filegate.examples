﻿using System;
using System.Globalization;
using System.Web;
using AG.FileGate.Client.Web.Models;

namespace AG.FileGate.Client.Web.Contracts
{
    public abstract class RestHttpHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                HttpHandlerResult result;
                string httpMethod = context.Request.HttpMethod;
                if (httpMethod.Equals("POST", StringComparison.OrdinalIgnoreCase))
                {
                    result = DoPost(context);
                }
                else if (httpMethod.Equals("GET", StringComparison.OrdinalIgnoreCase))
                {
                    result = DoGet(context);
                }
                else if (httpMethod.Equals("DELETE", StringComparison.OrdinalIgnoreCase))
                {
                    result = DoDelete(context);
                }
                else if (httpMethod.Equals("PUT", StringComparison.OrdinalIgnoreCase))
                {
                    result = DoPut(context);
                }
                else
                {
                    result = ThrowInvalidOperationException(context.Request.HttpMethod.ToUpper(CultureInfo.InvariantCulture));
                }
                HttpUtility.WriteResponse(context, result.Result, result.Status, ContentType);
            }
            catch (System.IO.EndOfStreamException)
            {
                string message =
                    FormatException(new ErrorInfo("Подключение с сервером разорвано из-за непредвиденной ошибки"));
                HttpUtility.WriteResponse(context, message, 500, ContentType);
            }
            catch (Exception ex)
            {
                HttpUtility.WriteResponse(context, FormatException(new ErrorInfo(ex)), 500, ContentType);
            }
        }

        protected abstract string ContentType { get; }

        protected virtual HttpHandlerResult DoPost(HttpContext context)
        {
            return ThrowInvalidOperationException("POST");
        }
    
        protected virtual HttpHandlerResult DoGet(HttpContext context)
        {
            return ThrowInvalidOperationException("GET");
        }
        protected virtual HttpHandlerResult DoDelete(HttpContext context)
        {
            return ThrowInvalidOperationException("DELETE");
        }
        protected virtual HttpHandlerResult DoPut(HttpContext context)
        {
            return ThrowInvalidOperationException("PUT");
        }

        protected abstract string FormatException(ErrorInfo ex);

        public bool IsReusable => false;

        private HttpHandlerResult ThrowInvalidOperationException(string method)
        {
            throw new InvalidOperationException("Http method \"" + method + "\" is not supported.");
        }
    }
}