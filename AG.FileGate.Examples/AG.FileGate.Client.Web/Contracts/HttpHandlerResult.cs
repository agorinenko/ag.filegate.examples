﻿namespace AG.FileGate.Client.Web.Contracts
{
    public abstract class HttpHandlerResult
    {
        public string Result => FormatResult(_result);
        public readonly int Status;

        private readonly object _result;

        public HttpHandlerResult(object result, int status)
        {
            _result = result;
            Status = status;
        }

        protected abstract string FormatResult(object result);
    }
}