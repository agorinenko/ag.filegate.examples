﻿using System;
using AG.FileGate.Client.Web.Models;

namespace AG.FileGate.Client.Web.Contracts
{
    public abstract class JsonHttpHandler : RestHttpHandler
    {
        protected override string ContentType => "application/json";

        protected override string FormatException(ErrorInfo ex)
        {
            ExceptionDataContract customExFromException = HttpUtility.CreateExceptionDataContract(ex);
            return HttpUtility.GetJson(customExFromException);
        }
    }
}