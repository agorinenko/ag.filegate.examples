﻿using AG.FileGate.Client.Api;
using AG.FileGate.Client.Web.Models;
using AG.FileGate.Interaction.Results;
using System;
using System.Collections.Generic;
using System.Web;

namespace AG.FileGate.Client.Web.Services
{
    public static class ItemsDelete
    {
        public static List<MessageDataContract> DoDelete(HttpContext context)
        {
            //true;#fileName.txt;@false;#folderName
            string[] itemCodes = context.GetRequiredParam("itemCodes").Split(Separators.Separator0, StringSplitOptions.RemoveEmptyEntries);           
            
            List<MessageDataContract> result = new List<MessageDataContract>();
            Transactions transaction = Utilities.MakeTransaction();
            foreach (string itemCode in itemCodes)
            {
                string[] itemParts = itemCode.Split(Separators.Separator1, StringSplitOptions.RemoveEmptyEntries);
                if (itemParts.Length == 2)
                {
                    string isFileStr = itemParts[0];
                    string serverRelativeUrl = itemParts[1];
                    bool isFile;
                    if (bool.TryParse(isFileStr, out isFile))
                    {
                        result.Add(DeleteItem(transaction, serverRelativeUrl, isFile));
                    }
                }
            }

            return result;
        }

        private static MessageDataContract DeleteItem(Transactions transaction, string serverRelativeUrl, bool isFile)
        {
            BooleanTransactionResult result = isFile ? transaction.DeleteFile(serverRelativeUrl) : transaction.DeleteDirectory(serverRelativeUrl);
            if (!result.Result)
            {
                return new MessageDataContract(result.Error, 0);
            }
            return new MessageDataContract("Элемент \"" + serverRelativeUrl + "\" удален", 1);
        }
    }
}
