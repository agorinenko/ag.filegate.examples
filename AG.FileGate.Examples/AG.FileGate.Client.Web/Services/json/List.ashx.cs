﻿using System.Collections.Generic;
using System.Net;
using System.Web;
using AG.FileGate.Client.Web.Contracts;
using AG.FileGate.Client.Web.Imp;
using AG.FileGate.Client.Web.Models;

namespace AG.FileGate.Client.Web.Services.json
{
    public class List : JsonHttpHandler
    {
        protected override HttpHandlerResult DoGet(HttpContext context)
        {
            ListData result = Services.List.DoGet(context);

            return new JsonHandlerResult(result, (int)HttpStatusCode.OK);
        }
    }
}