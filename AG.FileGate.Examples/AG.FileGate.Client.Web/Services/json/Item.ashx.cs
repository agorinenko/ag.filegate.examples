﻿using System.Net;
using System.Web;
using AG.FileGate.Client.Web.Contracts;
using AG.FileGate.Client.Web.Imp;
using AG.FileGate.Client.Web.Models;

namespace AG.FileGate.Client.Web.Services.json
{
    public class Item : JsonHttpHandler
    {
        protected override HttpHandlerResult DoGet(HttpContext context)
        {
            FileStorageItem result = Services.Item.DoGet(context);
            return new JsonHandlerResult(result, (int)HttpStatusCode.OK);
        }
    }
}
