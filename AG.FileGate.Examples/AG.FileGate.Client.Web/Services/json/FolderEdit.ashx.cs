﻿using System.Net;
using System.Web;
using AG.FileGate.Client.Web.Contracts;
using AG.FileGate.Client.Web.Imp;
using AG.FileGate.Client.Web.Models;

namespace AG.FileGate.Client.Web.Services.json
{
    public class FolderEdit : JsonHttpHandler
    {
        protected override HttpHandlerResult DoPut(HttpContext context)
        {
            MessageDataContract result = Services.FolderEdit.DoPut(context);
            return new JsonHandlerResult(result, (int)HttpStatusCode.OK);
        }
    }
}