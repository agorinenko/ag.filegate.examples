﻿using System.Collections.Generic;
using System.Net;
using System.Web;
using AG.FileGate.Client.Web.Contracts;
using AG.FileGate.Client.Web.Imp;
using AG.FileGate.Client.Web.Models;

namespace AG.FileGate.Client.Web.Services.json
{
    public class FileUpload : JsonHttpHandler
    {
        protected override HttpHandlerResult DoPost(HttpContext context)
        {
            List<MessageDataContract> result = Services.FileUpload.DoPost(context);

            return  new JsonHandlerResult(result, (int)HttpStatusCode.OK);
        }
    }
}