﻿using System.Net;
using System.Web;
using AG.FileGate.Client.Web.Contracts;
using AG.FileGate.Client.Web.Imp;
using AG.FileGate.Client.Web.Models;

namespace AG.FileGate.Client.Web.Services.json
{
    public class FileEdit : JsonHttpHandler
    {
        protected override HttpHandlerResult DoPut(HttpContext context)
        {
            MessageDataContract result = Services.FileEdit.DoPut(context);
            return new JsonHandlerResult(result, (int)HttpStatusCode.OK);
        }
    }
}