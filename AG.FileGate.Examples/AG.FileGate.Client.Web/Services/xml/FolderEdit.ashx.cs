﻿using System.Net;
using System.Web;
using AG.FileGate.Client.Web.Contracts;
using AG.FileGate.Client.Web.Imp;
using AG.FileGate.Client.Web.Models;

namespace AG.FileGate.Client.Web.Services.xml
{
    public class FolderEdit : XmlHttpHandler
    {
        protected override HttpHandlerResult DoPut(HttpContext context)
        {
            MessageDataContract result = Services.FolderEdit.DoPut(context);
            return new XmlHandlerResult(result, (int)HttpStatusCode.OK);
        }
    }
}