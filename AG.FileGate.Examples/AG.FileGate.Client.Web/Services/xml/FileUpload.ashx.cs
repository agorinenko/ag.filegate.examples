﻿using System.Collections.Generic;
using System.Net;
using System.Web;
using AG.FileGate.Client.Web.Contracts;
using AG.FileGate.Client.Web.Imp;
using AG.FileGate.Client.Web.Models;

namespace AG.FileGate.Client.Web.Services.xml
{
    public class FileUpload : XmlHttpHandler
    {
        protected override HttpHandlerResult DoPost(HttpContext context)
        {
            List<MessageDataContract> result = Services.FileUpload.DoPost(context);

            return new XmlHandlerResult(result, (int)HttpStatusCode.OK);
        }
    }
}