﻿using System.Collections.Generic;
using System.Net;
using System.Web;
using AG.FileGate.Client.Web.Contracts;
using AG.FileGate.Client.Web.Imp;
using AG.FileGate.Client.Web.Models;

namespace AG.FileGate.Client.Web.Services.xml
{
    public class List : XmlHttpHandler
    {
        protected override HttpHandlerResult DoGet(HttpContext context)
        {
            ListData result = Services.List.DoGet(context);
            return new XmlHandlerResult(result, (int)HttpStatusCode.OK);
        }
    }
}