﻿using System.Collections.Generic;
using System.Net;
using System.Web;
using AG.FileGate.Client.Web.Contracts;
using AG.FileGate.Client.Web.Imp;
using AG.FileGate.Client.Web.Models;

namespace AG.FileGate.Client.Web.Services.xml
{
    public class ItemsDelete : JsonHttpHandler
    {
        protected override HttpHandlerResult DoDelete(HttpContext context)
        {
            List<MessageDataContract> result = Services.ItemsDelete.DoDelete(context);

            return new JsonHandlerResult(result, (int)HttpStatusCode.OK);
        }
    }
}