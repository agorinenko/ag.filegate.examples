﻿using AG.FileGate.Client.Api;
using AG.FileGate.Client.Web.Models;
using AG.FileGate.Interaction.Results;
using System.Collections.Generic;
using System.Web;

namespace AG.FileGate.Client.Web.Services
{
    public static class FolderEdit
    {
        public static MessageDataContract DoPut(HttpContext context)
        {
            string key = context.GetKeyParameter();
            string newFolderName = context.GetRequiredParam("newFolderName");

            string parentFolder = Utilities.GetParentFolderByItemKey(key);
            string newServerRelativeUrl = $"{parentFolder.TrimEnd('/')}/{newFolderName.TrimStart('/')}";

            Dictionary<string, object> updatedProperties = new Dictionary<string, object>
            {
                {"ServerRelativeUrl", newServerRelativeUrl}
            };

            Transactions transaction = Utilities.MakeTransaction();
            BooleanTransactionResult result = transaction.UpdateDirectory(key, updatedProperties);

            if (!result.Result)
            {
                return new MessageDataContract(result.Error, 0);
            }
            return new MessageDataContract("Папка отредактирована", 1);
        }
    }
}
