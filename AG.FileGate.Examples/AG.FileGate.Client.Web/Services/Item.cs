﻿using System.Web;
using AG.FileGate.Client.Api;

namespace AG.FileGate.Client.Web.Services
{
    public static class Item
    {
        public static Models.FileStorageItem DoGet(HttpContext context)
        {
            Transactions transaction = Utilities.MakeTransaction();

            string key = context.GetKeyParameter();

            Interaction.Model.FileStorageItem item = transaction.GetItemByKey(key);

            return new Models.FileStorageItem(item);
        }
    }
}
