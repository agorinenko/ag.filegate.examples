﻿using System;
using System.IO;
using System.Web;
using System.Web.Hosting;
using System.Xml;
using AG.FileGate.Client.Api;
using AG.FileGate.Configuration;

namespace AG.FileGate.Client.Web.Services
{
    internal static class Utilities
    {
        private const char ParentKey = '/';

        public static string GetParentFolderByItemKey(string key)
        {
            key = key.TrimEnd(ParentKey);
            int last = key.LastIndexOf(ParentKey.ToString(), StringComparison.Ordinal);
            if (last > 0)
            {
                return key.Substring(0, last);
            }

            return ParentKey.ToString();
        }

        public static string GetKeyParameter(this HttpContext context)
        {
            string key = context.GetRequiredParam("key");
            return key;
        }

        public static uint GetFromParameter(this HttpContext context)
        {
            string fromStr = context.GetParam("from");
            uint from;
            const uint defaultFrom = 0;
            if (uint.TryParse(fromStr, out from))
            {
                return from;
            }
            return defaultFrom;
        }

        public static uint GetRowLimitParameter(this HttpContext context)
        {
            string limitStr = context.GetParam("limit");
            uint limit;
            const uint defaultLimit = 20;
            if (uint.TryParse(limitStr, out limit))
            {
                return limit;
            }
            return defaultLimit;
        }

        public static string GetParentFolderParameter(this HttpContext context)
        {
            string parentFolder = context.GetParam("folder");
            if (string.IsNullOrEmpty(parentFolder))
            {
                parentFolder = ParentKey.ToString();
            }
            return parentFolder;
        }

        public static string GetSearchPatternParameter(this HttpContext context)
        {
            string searchPattern = context.GetParam("search");
            if (string.IsNullOrEmpty(searchPattern))
            {
                searchPattern = "*";
            }
            return searchPattern;
        }
        
        public static Transactions MakeTransaction()
        {
            string clientConfigPath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data",
                    "client.xml");
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(clientConfigPath);

            XmlConfiguration xmlConfig = new XmlConfiguration(xDoc);
            Tcp.Configuration clientConfig = new Tcp.Configuration(xmlConfig);

            return new Transactions(clientConfig);
        }
    }
}