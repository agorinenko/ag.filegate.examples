﻿using AG.FileGate.Client.Api;
using AG.FileGate.Client.Web.Models;
using AG.FileGate.Interaction.Results;
using System.Web;

namespace AG.FileGate.Client.Web.Services
{
    public static class FolderAdd
    {
        public static MessageDataContract DoPost(HttpContext context)
        {
            string folderName = context.GetRequiredParam("folderName");           
            string parentFolder = context.GetParentFolderParameter();

            folderName = $"{parentFolder.TrimEnd('/')}/{folderName.TrimStart('/')}";
            
            Transactions transaction = Utilities.MakeTransaction();
            BooleanTransactionResult result = transaction.AddDirectory(folderName);

            if (!result.Result)
            {
                return new MessageDataContract(result.Error, 0);
            }
            return new MessageDataContract("Папка \"" + folderName + "\" добавлена", 1);
        }
    }
}
