﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using AG.FileGate.Client.Api;
using AG.FileGate.Client.Web.Models;
using AG.FileGate.Interaction.Results;

namespace AG.FileGate.Client.Web.Services
{
    public static class FileUpload
    {
        public static List<MessageDataContract> DoPost(HttpContext context)
        {
            HttpFileCollection files = context.Request.Files;
            if (0 == files.Count)
            {
                throw new FileNotFoundException("Файл не найден");
            }
            string parentFolder = context.GetParentFolderParameter();

            List<MessageDataContract> result = new List<MessageDataContract>();

            Transactions transaction = Utilities.MakeTransaction();
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                result.Add(AddHttpPostedFile(transaction, parentFolder, file));
            }

            return result;
        }

        private static MessageDataContract AddHttpPostedFile(Transactions transaction, string parentFolder, HttpPostedFile file)
        {
            string fileName = $"{parentFolder.TrimEnd('/')}/{file.FileName.TrimStart('/')}";

            BooleanTransactionResult result = transaction.AddFile(fileName, file.InputStream);
            if (!result.Result)
            {
                return new MessageDataContract(result.Error, 0);
            }
            return new MessageDataContract("Файл \"" + file.FileName + "\" загружен", 1);
        }
    }
}