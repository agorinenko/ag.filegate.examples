﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using AG.FileGate.Client.Api;
using AG.FileGate.Client.Web.Models;
using AG.FileGate.Interaction.Model;
using AG.FileGate.Utilities;

namespace AG.FileGate.Client.Web.Services
{
    public static class List
    {
        public static ListData DoGet(HttpContext context)
        {
            Transactions transaction = Utilities.MakeTransaction();

            string parentFolder = context.GetParentFolderParameter();
            string searchPattern = context.GetSearchPatternParameter();
            string where = $"ServerRelativeUrl={parentFolder}&SearchPattern={searchPattern}";
            uint from = context.GetFromParameter();
            uint limit = context.GetRowLimitParameter();

            FileStorageItemCollection items =  transaction.GetItems(new Query
            {
                Where = where,
                From = from,
                RowLimit = limit
            });

            return new ListData
            {
                Items = items.Select(item => new Models.FileStorageItem(item)),
                PagingInfo = items.PagingInfo
            };
        }
    }
}