﻿using System.Collections.Generic;
using System.Web;
using AG.FileGate.Client.Api;
using AG.FileGate.Client.Web.Models;
using AG.FileGate.Interaction.Results;

namespace AG.FileGate.Client.Web.Services
{
    public static class FileEdit
    {
        public static MessageDataContract DoPut(HttpContext context)
        {
            string key = context.GetKeyParameter();
            string newFileName = context.GetRequiredParam("newFileName");

            string parentFolder = Utilities.GetParentFolderByItemKey(key);
            string newServerRelativeUrl = $"{parentFolder.TrimEnd('/')}/{newFileName.TrimStart('/')}";

            Dictionary<string, object> updatedProperties = new Dictionary<string, object>
            {
                {"ServerRelativeUrl", newServerRelativeUrl}
            };

            Transactions transaction = Utilities.MakeTransaction();
            BooleanTransactionResult result = transaction.UpdateFile(key, updatedProperties);

            if (!result.Result)
            {
                return new MessageDataContract(result.Error, 0);
            }
            return new MessageDataContract("Файл отредактирован", 1);
        }
    }
}