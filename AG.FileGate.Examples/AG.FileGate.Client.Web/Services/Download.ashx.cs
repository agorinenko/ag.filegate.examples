﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Web;
using AG.FileGate.Client.Api;

namespace AG.FileGate.Client.Web.Services
{
    /// <summary>
    /// Summary description for Download
    /// </summary>
    public class Download : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string httpMethod = context.Request.HttpMethod;
            if (httpMethod.Equals("GET", StringComparison.OrdinalIgnoreCase))
            {
                string key = HttpUtility.GetParam(context, "key");

                context.Response.Clear();
                context.Response.ClearHeaders();
                context.Response.ClearContent();

                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                context.Response.BufferOutput = true;

                context.Response.ContentType = "application/octet-stream";
                //context.Response.AddHeader("Content-Length", length.ToString());
                context.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + Path.GetFileName(key) + "\"");

                Transactions transaction = Utilities.MakeTransaction();
                transaction.GetFileStream(key, bytes =>
                {
                    context.Response.BinaryWrite(bytes);
                });
                context.Response.StatusCode = 200;
                context.Response.Flush();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            else
            {
                throw new InvalidOperationException("Http method \"" + httpMethod + "\" is not supported.");
            }
        }

        public bool IsReusable => false;
    }
}