﻿using System;
using AG.FileGate.Client.Web.Contracts;

namespace AG.FileGate.Client.Web.Imp
{
    public sealed class JsonHandlerResult : HttpHandlerResult
    {
        public JsonHandlerResult(object result, int status) : base(result, status)
        {
        }

        protected override string FormatResult(object result)
        {
            return HttpUtility.GetJson(result);
        }
    }
}