﻿using System.Runtime.Serialization;
using AG.FileGate.Utilities.Enums;
using System;
using System.Globalization;

namespace AG.FileGate.Client.Web.Models
{
    [DataContract]
    public class FileStorageItem
    {
        private readonly Interaction.Model.FileStorageItem _item;

        [DataMember(Name = "key")]
        public string ServerRelativeUrl { get; set; }
        [DataMember(Name = "name")]
        public string Name { get; set; }

        private string _itemType;
        [DataMember(Name = "type")]
        public string ItemType
        {
            get
            {
                if (string.IsNullOrEmpty(_itemType))
                {
                    _itemType = _item.FileStorageItemType == FileStorageItemTypes.Dir ? "dir" : "file";
                }

                return _itemType;
            }
            set { _itemType = value; }
        }

        private decimal? _length;
        [DataMember(Name = "size")]
        public decimal Length
        {
            get
            {
                if (null == _length)
                {
                    _length = Math.Round(_item.Length, 2);
                }

                return _length.Value;
            }
            set { _length = value; }
        }

        private string _created;
        [DataMember(Name = "created")]
        public string Created
        {
            get
            {
                if (string.IsNullOrEmpty(_created))
                {
                    _created = GetLocalDateTime(_item.CreatedUtc);
                }

                return _created;
            }
            set { _created = value; }
        }

        private string _modified;
        [DataMember(Name = "modified")]
        public string Modified
        {
            get
            {
                if (string.IsNullOrEmpty(_modified))
                {
                    _modified = GetLocalDateTime(_item.ModifiedUtc);
                }

                return _modified;
            }
            set { _modified = value; }
        }

        //[DataMember(Name = "ext")]
        //public string Extension { get; set; }

        public FileStorageItem(Interaction.Model.FileStorageItem item)
        {
            _item = item;

            ServerRelativeUrl = item.ServerRelativeUrl;
            //ItemType = item.FileStorageItemType == FileStorageItemTypes.Dir ? "dir" : "file";
            //Length = Math.Round(item.Length, 2);
            Name = item.Name;
            //string extension = Path.GetExtension(ServerRelativeUrl);
            //if (extension != null)
            //{
            //    Extension = item.FileStorageItemType == FileStorageItemTypes.Dir ? "dir" : extension.TrimStart('.');
            //}
            //else
            //{
            //    Extension = "undef";
            //}
        }

        private string GetLocalDateTime(DateTime utcDateTime)
        {
            CultureInfo currentCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
            DateTimeFormatInfo currentDateTimeFormat = currentCulture.DateTimeFormat;
            TimeZoneInfo toTimeZone = TimeZoneInfo.Local;

            DateTime converted = TimeZoneInfo.ConvertTimeFromUtc(utcDateTime, toTimeZone);
            string date = converted.ToString(currentDateTimeFormat.ShortDatePattern, currentCulture);
            string time = converted.ToString(currentDateTimeFormat.ShortTimePattern, currentCulture);
            return $"{date} {time}";
        }
    }
}