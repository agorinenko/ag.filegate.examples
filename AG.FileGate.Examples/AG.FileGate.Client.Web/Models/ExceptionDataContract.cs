﻿using System;
using System.Runtime.Serialization;

namespace AG.FileGate.Client.Web.Models
{
    [DataContract]
    public class ExceptionDataContract
    {
        [DataMember(Name = "message")]
        public string Message { get; set; }
        [DataMember(Name = "id")]
        public Guid Id { get; set; }
    }
}