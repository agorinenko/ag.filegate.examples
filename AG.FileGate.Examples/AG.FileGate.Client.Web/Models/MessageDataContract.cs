﻿using System.Runtime.Serialization;

namespace AG.FileGate.Client.Web.Models
{
    [DataContract]
    public class MessageDataContract
    {
        [DataMember(Name = "message")]
        public string Message { get; set; }

        [DataMember(Name = "code")]
        public int Code { get; set; }

        public MessageDataContract(string message, int code)
        {
            Message = message;
            Code = code;
        }
    }
}