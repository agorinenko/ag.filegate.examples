﻿using System;

namespace AG.FileGate.Client.Web.Models
{
    public class ErrorInfo
    {
        public readonly string Message;

        public ErrorInfo(string message)
        {
            Message = message;
        }
        public ErrorInfo(Exception ex)
        {
            Message = ex.Message;
        }
    }
}