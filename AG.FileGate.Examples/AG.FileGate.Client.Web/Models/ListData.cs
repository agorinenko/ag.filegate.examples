﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using AG.FileGate.Utilities;

namespace AG.FileGate.Client.Web.Models
{
    [DataContract]
    public class ListData
    {
        [DataMember(Name = "items")]
        public IEnumerable<FileStorageItem> Items;
        [DataMember(Name = "paging")]
        public PagingInfo PagingInfo;
    }
}