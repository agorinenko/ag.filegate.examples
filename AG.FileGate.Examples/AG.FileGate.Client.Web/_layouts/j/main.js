﻿/// <reference path="ui/file.upload.js" />
define([
    "jquery",
    "bootstrap",

    "utils",
    "client",
    "messages",
    "modals",
    
    "app/ui/file.upload",
    "app/ui/search.main.form",
    "app/ui/folder.add",
    "app/ui/main.list"
], function ($, b, utils, client, messages, modals,
    fileUpload, searchMainForm, folderAdd, mainList) {
    return {
        $agmessage: null,
        items: null,
        start: function () {
            var self = this;

            self.$agmessage = $("#agmessage");

            self.items = new Array(fileUpload, searchMainForm, folderAdd, mainList);

            self.sendNotif("init", self);
            //self.init();
        },
        init: function () {
            var self = this;

            $.each(self.items, function(key,value) {
                value.init(self);
            });
        },
        getCurrentFolder: function () {
            return mainList.getCurrentFolder();
        },
        setCurrentFolder: function (value) {
            mainList.setCurrentFolder(value);
        },
        getSearchPattern: function () {
            return mainList.getSearchPattern();
        },
        setSearchPattern: function (value) {
            mainList.setSearchPattern(value);
        },
        showSuccessMessage: function (result) {
            var result200 = utils.formatResult200(result);
            messages.__createMessage(this.$agmessage, result200, 6000);
        },
        showErrorMessage: function (err) {
            var errMessage = utils.getDefaultErrorMessage(err);
            messages.error(this.$agmessage, "Ошибка", errMessage, 6000);
        },
        hideErrorMessage: function () {
            messages.hide(this.$agmessage);
        },
        refreshListView: function (openBlock) {
            mainList.refreshListView(openBlock);
        },
        sendNotif: function(name, args) {
            var self = this;

            $.each(self.items, function (key, value) {
                if (value[name]) {
                    value[name](args);
                }
            });
        }
    };
});