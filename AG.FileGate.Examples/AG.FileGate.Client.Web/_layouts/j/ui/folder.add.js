﻿define([
    "jquery",
    "client",
    "modals",
    "messages",
    "utils"
], function ($, client, modals, messages, utils) {
    return {
        $createFolderBtn: null,
        init: function (app) {
            var self = this;

            self.$createFolderBtn = $("#createFolderBtn");

            self.$createFolderBtn.on("click", function (e) {
                e.preventDefault();
                
                modals.open({
                    id: "folderAdd",
                    title: "Новая папка",
                    allowsave: true,
                    url: "_layouts/t/folder.add.html",
                    onSave: function (e, $dialog) {
                        e.preventDefault();
                        modals.clearFormGroupErrors($dialog);
                        var $folderName = $dialog.find("#folderName"),
                            folderName = $.trim($folderName.val());

                        if (!folderName.length || folderName.length > 250) {
                            modals.riseFormGroupError($folderName);
                            return;
                        }

                        var $block = modals.block();

                        client.folderAdd(app.getCurrentFolder(), folderName, function (result) {                           
                            modals.close($dialog);

                            app.showSuccessMessage(result);
                            app.refreshListView(false);
                        }, function (err) {
                            var $modalMessage = modals.getModalMessage($dialog),
                            errMessage = utils.getDefaultErrorMessage(err);

                            messages.error($modalMessage, "Ошибка", errMessage);
                        }).always(function () {
                            modals.close($block);                            
                        });
                    }
                });
            });           
        }   
    };
});