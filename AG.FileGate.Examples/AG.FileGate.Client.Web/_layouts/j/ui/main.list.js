﻿define([
    "jquery",
    "utils",
    "client",
    "modals",
    "../lib/crumbs",
    "./folder.update",
    "./file.update",
    "text!t/list.html"
], function ($, utils, client, modals, crumbs, folderUpdate, fileUpdate, listTemplate) {
    return {
        currentFolder: "/",
        searchPattern: "",
        $aglist: null,
        globalApp: null,
        getCurrentFolder: function() {
            return this.currentFolder;
        },
        setCurrentFolder: function (value) {
            this.currentFolder = value;
        },
        getSearchPattern: function () {
            return this.searchPattern;
        },
        setSearchPattern: function (value) {
            this.searchPattern = value;
        },
        init: function (app) {
            var self = this;

            self.globalApp = app;

            crumbs.init();

            self.$aglist = $("#aglist");
     
            self.$aglist.on("click", "a[data-action='go']", function (e) {
                e.preventDefault();

                var $this = $(this),
                    key = $this.data("key"),
                    name = $this.text();

                if (key.length) {
                    crumbs.add(key, name);

                    app.sendNotif("clearState");
                    app.hideErrorMessage();

                    self.searchPattern = "";
                    self.currentFolder = key;
                    self.refreshListView(true);
                }
            }).on("click", "a[data-action='delete']", function (e) {
                e.preventDefault();

                var $this = $(this),
                    isFile = $this.data("isfile"),
                    key = $this.data("key");

                if (confirm("Вы действительно хотите выполнить удаление элемента?") && key.length) {
                    var item = {
                        isFile: isFile,
                        name: key
                    },
                    $dialog = modals.block();

                    client.itemsDelete(item, function (result) {
                        app.showSuccessMessage(result);
                        self.refreshListView(false);
                    }, function (err) {
                        app.showErrorMessage(err);
                    }).always(function () {
                        modals.close($dialog);
                    });
                }
            }).on("click", "a[data-action='update']", function (e) {
                e.preventDefault();

                var $this = $(this),
                    isFile = $this.data("isfile"),
                    key = $this.data("key");

                if (!isFile) {
                    folderUpdate.init(app, {
                        "key": key
                    });
                } else {
                    fileUpdate.init(app, {
                        "key": key
                    });
                }
            });

            self.refreshListView(true);
        },
        refreshListView: function (openBlock) {
            var self = this,
                $dialog;
            
            if (openBlock) {
                $dialog = modals.block();
            }

            client.list(self.currentFolder, self.searchPattern, function (result) {
                var html = utils.template(listTemplate, {
                    items: result.items,
                    crumbs: crumbs.getAll()
                });

                self.$aglist.html(html);

                if (openBlock) {
                    modals.close($dialog);
                }
            }, function(err) {
                self.globalApp.showErrorMessage(err);
            });
        }
    };
});