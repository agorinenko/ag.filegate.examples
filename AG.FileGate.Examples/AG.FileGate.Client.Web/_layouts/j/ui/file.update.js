﻿define([
    "jquery",
    "client",
    "modals",
    "messages",
    "utils"
], function ($, client, modals, messages, utils) {
    return {
        init: function(app, data) {
            var self = this,
                parseFilePath = function (path) {
                    var i = path.lastIndexOf('.'),
                        title, extension;

                    if (i > 0) {
                        title = path.substr(0, i);
                        extension = path.substr(i);
                    } else {
                        title = path;
                        extension = '';
                    }

                    return {
                        "title": title,
                        "extension": extension
                    };
                };

            modals.open({
                id: "folderUpdate",
                title: "Редактирование файла",
                allowsave: true,
                url: "_layouts/t/file.add.html",
                formData: data,
                onDataBinding: function (formData, $dialog) {
                    client.item(formData.key, function (result) {
                        var $fileName = $dialog.find("#fileName"),
                            $extension = $dialog.find("#extension"),
                            $key = $dialog.find("#key"),
                            path = parseFilePath(result.name);

                        $fileName.val(path.title);
                        $extension.text(path.extension);
                        $key.val(result.key);
                    }, function (err) {
                        app.showErrorMessage(err);
                    });
                },
                onUpdate: function(e, $dialog) {
                    e.preventDefault();
                    modals.clearFormGroupErrors($dialog);
                    var $fileName = $dialog.find("#fileName"),
                        fileName = $.trim($fileName.val()),
                        $extension = $dialog.find("#extension"),
                        extension = $.trim($extension.text()),
                        $key = $dialog.find("#key"),
                        key = $.trim($key.val());

                    if (!key.length) {
                        var $modalMessage = modals.getModalMessage($dialog);
                        messages.error($modalMessage, "Ошибка", "Элемент не определен");
                        return;
                    }

                    if (!fileName.length || fileName.length > 250) {
                        modals.riseFormGroupError($fileName);
                        return;
                    }

                    fileName = fileName + extension;

                    var $block = modals.block();

                    client.fileEdit(key, fileName, function (result) {
                        modals.close($dialog);

                        app.showSuccessMessage(result);
                        app.refreshListView(false);
                    }, function (err) {
                        var $modalMessage = modals.getModalMessage($dialog),
                            errMessage = utils.getDefaultErrorMessage(err);

                        messages.error($modalMessage, "Ошибка", errMessage);
                    }).always(function() {
                        modals.close($block);
                    });
                }
            });
        }
    };
});