﻿define([
    "jquery",
    "client",
    "modals"
], function ($, client, modals) {
    return {
        init: function (app) {
            var $uploadBtn = $("#uploadBtn"),
                $filePicker = $("#filePicker");

            $uploadBtn.on("click", function (e) {
                e.preventDefault();

                var files = $filePicker.get(0).files,
                    $dialog = modals.block();

                client.fileUpload(app.getCurrentFolder(), files, function (result) {
                    $filePicker.val("");

                    app.showSuccessMessage(result);
                    app.refreshListView(false);
                }, function(err) {
                    app.showErrorMessage(err);
                }).always(function () {
                    modals.close($dialog);
                });
            });           
        }   
    };
});