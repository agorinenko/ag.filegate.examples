﻿define([
    "jquery",
    "utils"
], function ($, utils) {
    return {
        $agsearchBtn: null,
        $agsearch: null,
        globalApp: null,
        clearState: function () {
            var self = this;

            self.$agsearch.val('');
        },
        init: function (app) {
            var self = this;

            self.globalApp = app;

            self.$agsearchBtn = $("#agsearchBtn");
            self.$agsearch = $("#agsearch");

            self.$agsearch.on("keypress", function (e) {
                var code = e.keyCode || e.which;
                if (13 === code) {
                    e.preventDefault();
                    utils.delay(function () {
                        self.__search();
                    }, 300);
                }
            });

            self.$agsearchBtn.on("click", function (e) {
                e.preventDefault();
                self.__search();
            });
        },
        __search: function () {
            var self = this;

            self.globalApp.setSearchPattern(self.$agsearch.val() + "*");
            self.globalApp.refreshListView(false);
        }
    };
});