﻿define([
    "jquery",
    "client",
    "modals",
    "messages",
    "utils"
], function ($, client, modals, messages, utils) {
    return {
        init: function(app, data) {
            var self = this;

            modals.open({
                id: "folderUpdate",
                title: "Редактирование папки",
                allowsave: true,
                url: "_layouts/t/folder.add.html",
                formData: data,
                onDataBinding: function (formData, $dialog) {
                    client.item(formData.key, function (result) {
                        var $folderName = $dialog.find("#folderName");
                        $folderName.val(result.name);

                        var $key = $dialog.find("#key");
                        $key.val(result.key);
                    }, function (err) {
                        app.showErrorMessage(err);
                    });
                    
                },
                onUpdate: function(e, $dialog) {
                    e.preventDefault();
                    modals.clearFormGroupErrors($dialog);
                    var $folderName = $dialog.find("#folderName"),
                        folderName = $.trim($folderName.val()),
                        $key = $dialog.find("#key"),
                        key = $.trim($key.val());

                    if (!key.length) {
                        var $modalMessage = modals.getModalMessage($dialog);
                        messages.error($modalMessage, "Ошибка", "Элемент не определен");
                        return;
                    }

                    if (!folderName.length || folderName.length > 250) {
                        modals.riseFormGroupError($folderName);
                        return;
                    }

                    var $block = modals.block();

                    client.folderEdit(key, folderName, function (result) {
                        modals.close($dialog);

                        app.showSuccessMessage(result);
                        app.refreshListView(false);
                    }, function (err) {
                        var $modalMessage = modals.getModalMessage($dialog),
                            errMessage = utils.getDefaultErrorMessage(err);

                        messages.error($modalMessage, "Ошибка", errMessage);
                    }).always(function() {
                        modals.close($block);
                    });
                }
            });
        }
    };
});