﻿define([],
    function () {
        var module = {
            _crumbs: [],
            init: function() {
                this._crumbs.push({
                    key: "/",
                    name: "Главная"
                });
            },
            add: function (key, name) {
                this.removeAfterKey(key);
                this._crumbs.push({
                    key: key,
                    name: name
                });
            },
            removeAfterKey: function (key) {
                var i = this.getIndexByKey(key),
                    l;
                if (i >= 0) {
                    l = this._crumbs.length - i;
                    this._crumbs.splice(i, l);
                }
            },
            getIndexByKey: function (key) {
                var self = this,
                    i = -1;
                $.each(self._crumbs, function(index, item) {
                    if (key === item.key) {
                        i = index;
                        return false;
                    }
                    return true;
                });

                return i;
            },
            getAll: function() {
                return this._crumbs;
            }
        };
        return module;
    }
);