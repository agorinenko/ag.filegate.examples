﻿define([
    "jquery"
],
    function ($, messages) {
        var module = {
            cache: {},
            getDefaultErrorMessage: function (err) {
                var errMessage;

                if (err.status !== 500 && err.statusText.length) {
                    errMessage = err.statusText + "(" + err.status + ")";
                } else if (err && err.responseJSON && err.responseJSON.message && err.responseJSON.message.length) {
                    errMessage = err.responseJSON.message;
                } else {
                    errMessage = "Непредвиденная ошибка";
                }

                return errMessage;
            },
            template: function (str, data) {
                var fn = !/\W/.test(str) ?
                    module.cache[str] = module.cache[str] || module.template(document.getElementById(str).innerHTML) :
                    new Function("data",
                            "var p=[],print=function(){p.push.apply(p,arguments);};" +
                            "with(data){p.push('" +

                            str
                                .replace(/[\r\t\n]/g, " ")
                                .split("<%").join("\t")
                                .replace(/((^|%>)[^\t]*)'/g, "$1\r")
                                .replace(/\t=(.*?)%>/g, "',$1,'")
                                .split("\t").join("');")
                                .split("%>").join("p.push('")
                                .split("\r").join("\\'")
                            + "');}return p.join('');");

                return data ? fn(data) : fn;
            },
            delay: function(callback, ms) {
                var timer = 0;
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
                return timer;
            },
            formatResult200(result) {
                var stringBuilder = "",
                    title,
                    state,
                    successCount = 0,
                    errorCount = 0;

                if ($.isArray(result)) {
                    var l = result.length,
                        lprev = l - 1;

                    for (var i = 0; i < l; i++) {
                        var item = result[i];

                        if (0 === item.code) {
                            errorCount++;
                        } else {
                            successCount++;
                        }

                        stringBuilder += item.message;
                        if (i !== lprev) {
                            stringBuilder += "<br />";
                        }
                    }
                } else {
                    stringBuilder = result.message;
                    if (0 === result.code) {
                        errorCount++;
                    } else {
                        successCount++;
                    }
                }

                if (0 === errorCount && successCount > 0) {
                    title = "Успех";
                    state = "success";
                } else if (errorCount > 0 && successCount > 0) {
                    title = "Не все операции выполненны успешно";
                    state = "warning";
                } else {
                    title = "Ошибка";
                    state = "danger";
                }

                return {
                    message: stringBuilder,
                    title: title,
                    state: state
                }
            },
            //true;#fileName.txt;@false;#folderName
            itemCodesToString: function (itemCodes) {
                if (itemCodes) {
                    var formatOnePart = function(o) {
                        return o.isFile + ";#" + o.name;
                    },
                    formatAllParts = function (items) {
                        var result = "";

                        for (var i = 0; i < items.length; i++) {
                            result += ";@" + formatOnePart(items[i]);
                        }

                        if (result.length) {
                            result = result.substr(2);
                        }

                        return result;
                    };
                    if (itemCodes instanceof Array) {
                        return formatAllParts(itemCodes);
                    } else if (itemCodes instanceof Object) {
                        return formatOnePart(itemCodes);
                    } 
                }
                return "";
            }
        };
        return module;
    }
);