﻿define([
    "jquery",
    "utils",
    "text!./t/message.html"
],
    function ($, utils, messageTemplate) {
        var module = {
            __createMessage: function ($parent, options, delay) {
                var html = utils.template(messageTemplate, options);

                $parent.empty().html(html);
                
                if (this.__isDelay(delay)) {
                    this.hide($parent, delay);
                }
            },
            __isDelay: function (delay) {
                return (delay && delay > 0);
            },
            hide: function ($parent, delay) {
                if (!delay) {
                    delay = 0;
                }

                utils.delay(function () {
                    if ($parent) {
                        $parent.empty();
                    }
                }, delay);
            },
            success: function ($parent, title, message, delay) {
                var self = this;

                self.__createMessage($parent, {
                    state: "success",
                    title: title,
                    message: message
                }, delay);
            },
            info: function ($parent, title, message, delay) {
                var self = this;

                self.__createMessage($parent, {
                    state: "info",
                    title: title,
                    message: message
                }, delay);
            },
            warning: function ($parent, title, message, delay) {
                var self = this;

                self.__createMessage($parent, {
                    state: "warning",
                    title: title,
                    message: message
                }, delay);
            },
            error: function ($parent, title, message, delay) {
                var self = this;

                self.__createMessage($parent, {
                    state: "danger",
                    title: title,
                    message: message
                }, delay);
            }
        };
        return module;
    }
);