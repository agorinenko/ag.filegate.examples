﻿define([
    "jquery",
    "utils",    
    "text!./t/dialog.html"
],
    function ($, utils, dialogTemplate) {
        var module = {
            getModalMessage: function ($dialog) {
                var $modalMessage = $dialog.find(".modal-dialog > .modal-content > .modal-message");

                return $modalMessage;
            },
            clearFormGroupErrors: function ($dialog) {
                $dialog.find(".form-group.has-error").removeClass("has-error");
            },
            riseFormGroupError: function($input) {
                $input.parent(".form-group").addClass("has-error");
            },
            close: function ($dialog, delay) {
                var hideAndRemove = function($d) {
                    if ($d) {
                        $d.modal("hide");
                        $d.remove();
                    }
                };

                if (!delay) {
                    delay = 0;
                }

                if (delay > 0) {
                    utils.delay(function() {
                        hideAndRemove($dialog);
                    }, delay);
                } else {
                    hideAndRemove($dialog);
                }
            },
            open: function (options) {
                this.__detectModal(options);

                var html = utils.template(dialogTemplate, options),
                    url = options.url,
                    isUrl = (url && url.length),
                    $dialog = $(html),
                    onShowBsModal = function(event, $d) {
                        if (options.allowsave) {
                            var $saveBtn = $d.find(".modal-dialog > .modal-content > .modal-footer > [data-save='true']");

                            if ($saveBtn.length) {
                                $saveBtn.on("click", function (e) {
                                    if (options.onSave) {
                                        options.onSave(e, $d);
                                    } else if (options.onUpdate) {
                                        options.onUpdate(e, $d);
                                    }
                                });
                            }
                        }

                        if (options.onDataBinding) {
                            options.onDataBinding(options.formData, $d);
                        }

                        if (options.onShowBsModal) {
                            options.onShowBsModal(event, $d);
                        }
                    };

                $dialog.on('show.bs.modal', function(event) {
                    var $d = $(this),
                        $modalBody;

                    if (isUrl) {
                        $.get(url, function(data) {
                            $modalBody = $d.find(".modal-dialog > .modal-content > .modal-body");
                            if ($modalBody.length) {
                                $modalBody.html(data);
                            }

                            onShowBsModal(event, $d);
                        });
                    } else {
                        onShowBsModal(event, $d);
                    }
                });
                if (options.onShownBsModal) {
                    $dialog.on('shown.bs.modal', function(event) {
                        options.onShownBsModal(event);
                    });
                }

                if (options.onHideBsModal) {
                    $dialog.on('hide.bs.modal', function(event) {
                        options.onHideBsModal(event);
                    });
                }

                if (options.onHiddenBsModal) {
                    $dialog.on('hidden.bs.modal', function(event) {
                        options.onHiddenBsModal(event);
                    });
                }

                if (options.onLoadedBsModal) {
                    $dialog.on('loaded.bs.modal', function(event) {
                        options.onLoadedBsModal(event);
                    });
                }

                $dialog.modal(options).show();

                return $dialog;
            },
            block: function () {
                return this.open({
                    id: "blockmodal",
                    fade: false,
                    title: "Загрузка",
                    allowclose: false,
                    backdrop: "static",
                    keyboard: false,
                    message: "Загрузка данных. Это не должно занять много времени..."
                });
            },
            __getMessage: function (options) {
                var url = options.url,
                    dfd = $.Deferred();

                if (url && url.length) {
                    $.get(url, function (data) {
                        options.message = data;
                    });
                } 

                return dfd.resolve(options).promise();
            },
            __detectModal: function (options) {
                if (options.id) {
                    var $oldItem = $("#" + options.id);
                    if ($oldItem) {
                        this.close($oldItem);                        
                    }
                }
            }
        };
        return module;
    }
);