﻿/// <reference path="utils.js" />
define([
    "jquery",
    "utils"
], function ($, utils) {
    return {
        __call: function (url, type, dataType, data, onSuccess, onError) {
            var formData;

            if (!(data instanceof FormData) && "GET" !== type.toUpperCase()) {
                formData = new FormData();
                for (var prop in data) {
                    if (formData.hasOwnProperty(prop)) {
                        formData.append(prop, data[prop]);
                    }
                }
            } else {
                formData = data;
            }

            var processData = formData instanceof FormData ? false : true;

            return $.ajax({
                url: url,
                type: type,
                dataType: dataType,
                data: formData,
                contentType: false,
                processData: processData,
                success: function (result) {
                    if (onSuccess) {
                        onSuccess(result);
                    }
                },
                error: function (err) {
                    if (onError) {
                        onError(err);
                    }
                }
            });
        },
        __json: function (url, type, formData, onSuccess, onError) {
            return this.__call(url, type, "json", formData, onSuccess, onError);
        },
        folderEdit: function (key, newFolderName, onSuccess, onError) {
            var formData = new FormData();

            formData.append("key", encodeURIComponent(key));
            formData.append("newFolderName", encodeURIComponent(newFolderName));

            return this.__json("/Services/json/FolderEdit.ashx", "PUT", formData, onSuccess, onError);
        },
        fileEdit: function (key, newFileName, onSuccess, onError) {
            var formData = new FormData();

            formData.append("key", encodeURIComponent(key));
            formData.append("newFileName", encodeURIComponent(newFileName));

            return this.__json("/Services/json/FileEdit.ashx", "PUT", formData, onSuccess, onError);
        },
        itemsDelete: function (itemCodes, onSuccess, onError) {
            var formData = new FormData();
            
            formData.append("itemCodes", encodeURIComponent(utils.itemCodesToString(itemCodes)));

            return this.__json("/Services/json/ItemsDelete.ashx", "DELETE", formData, onSuccess, onError);
        },
        folderAdd: function (parentFolder, folderName, onSuccess, onError) {
            var formData = new FormData();

            formData.append("folder", encodeURIComponent(parentFolder));
            formData.append("folderName", encodeURIComponent(folderName));

            return this.__json("/Services/json/FolderAdd.ashx", "POST", formData, onSuccess, onError);
        },
        fileUpload: function (parentFolder, files, onSuccess, onError) {
            var formData = new FormData();

            formData.append("folder", encodeURIComponent(parentFolder));

            $.each(files, function (key, file) {
                formData.append(file.name, file);
            });

            return this.__json("/Services/json/FileUpload.ashx", "POST", formData, onSuccess, onError);
        },
        list: function (parentFolder, searchPattern, onSuccess, onError) {
            var formData = {
                "folder": encodeURIComponent(parentFolder),
                "search": encodeURIComponent(searchPattern)
            };

            return this.__json("/Services/json/List.ashx", "GET", formData, onSuccess, onError);
        },
        item: function (key, onSuccess, onError) {
            var formData = {
                "key": encodeURIComponent(key)
            };

            return this.__json("/Services/json/Item.ashx", "GET", formData, onSuccess, onError);
        }
    };
});