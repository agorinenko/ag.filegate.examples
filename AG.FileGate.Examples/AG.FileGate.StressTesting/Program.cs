﻿using System;
using System.IO;
using System.Threading;
using AG.FileGate.Client.Tcp;
using AG.FileGate.Interaction.Results;
using AG.FileGate.Interaction.Tcp.Transactions;
using AG.FileGate.TestUtilities;

namespace AG.FileGate.StressTesting
{
    class Program
    {
        private static Client.Tcp.Configuration _clientConfig;
        static void Main(string[] args)
        {
            _clientConfig = new Client.Tcp.Configuration(ConfigurationUtils.GetXmlConfigurationFromBaseDirectory("client.xml"));

            string dirName = "StressTesting";
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Client is starting...");
                Thread t = new Thread(Delegate =>
                {
                    try
                    {
                        string fileName = Path.GetRandomFileName();
                        string serverRelativeUrl = $"/{dirName}/{fileName}";
                        BooleanTransactionResult result = AddAviFile(serverRelativeUrl);

                        Console.WriteLine(!result.Result ? result.Error : "File was added");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                });
                t.Start();
            }
            for (int i = 0; i < 1000; i++)
            {
                Console.WriteLine("Client is starting...");
                Thread t = new Thread(Delegate =>
                {
                    try
                    {
                        string fileName = Path.GetRandomFileName();
                        string serverRelativeUrl = $"/{dirName}/{fileName}";
                        BooleanTransactionResult result = AddSmallFile(serverRelativeUrl);

                        Console.WriteLine(!result.Result ? result.Error : "File was added");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                });
                t.Start();
            }
        }

        private static BooleanTransactionResult AddSmallFile(string serverRelativeUrl)
        {
            return AddFile(serverRelativeUrl, "small.txt");
        }

        private static BooleanTransactionResult AddAviFile(string serverRelativeUrl)
        {
            return AddFile(serverRelativeUrl, "big.avi");
        }

        private static BooleanTransactionResult AddFile(string serverRelativeUrl, string shortFileName)
        {
            BooleanTransactionResult result;

            string testedFile = DataUtils.GetFileNameFromBaseDirectory(shortFileName);
            using (Stream file = new FileStream(testedFile, FileMode.Open, FileAccess.Read))
            {
                Client.Contracts.Client externalClient = new NetClient(_clientConfig);
                externalClient.EstablishConnection();
                try
                {
                    Client.Tcp.Transactions.RootTransactionsFactory factory =
                        new Client.Tcp.Transactions.RootTransactionsFactory();
                    ClientNetTransaction fileStreamAddFileTransaction =
                        factory.MakeAddFileTransaction(serverRelativeUrl, file);

                    result =
                        (BooleanTransactionResult) externalClient.Execute(fileStreamAddFileTransaction);
                }
                finally
                {
                    externalClient.CloseConnection();
                }
            }

            return result;
        }
    }
}
